EAPI=7
DESCRIPTION="Meta package to install Nextcloud using Lighttpd as frontend"
HOMEPAGE="https://gitlab.com/mapmot/bleeding-overlay"
SRC_URI=""
S="${WORKDIR}"
#LICENSE=""
SLOT="8.0"
KEYWORDS="~amd64 ~arm"
#RESTRICT="strip"
RDEPEND="www-apps/nextcloud
	www-servers/lighttpd
	dev-php/pecl-imagick"
#DEPEND="${RDEPEND}"
#BDEPEND=""
src_install() {
	insinto /etc/php/fpm-php8.0/fpm.d
	newins "${FILESDIR}/nextcloud-php-fpm.conf" nextcloud.conf
}

